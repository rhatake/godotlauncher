__author__ = 'ryoh'

import threading
from urllib.request import urlopen
import time


class Download():
    _total_size = 0
    _completed = 0
    _temp_file = None
    _lock = None
    
    _time_last_call = 0
    _completed_last_call = 0
    _speed = 0

    file_name = None
    destination = None
    canceled = False

    def __init__(self, file_name, save_destination, size):
        self._file_name = file_name
        self._total_size = size
        self.destination = save_destination
        self._temp_file = open(save_destination + '/' + self._file_name, 'wb')
        self._lock = threading.Lock()
        self._time_last_call = time.time()

    def append_data(self, data):
        if not self.completed():
            self._temp_file.write(data)
            self._completed += len(data)

    def get_percentage(self):
        return round((float(self._completed) / self._total_size) * 100, 2)

    def get_speed(self):
        deltatime = time.time() - self._time_last_call

        if deltatime == 0:
            return self._speed
        
        deltadata = self._completed - self._completed_last_call
        
        self._time_last_call = time.time()
        self._completed_last_call = self._completed
        self._speed = float(deltadata) / deltatime
        
        return self._speed

    def completed(self):
        if self._completed == self._total_size:
            return True
        else:
            return False

    def cancel_download(self):
        if not self._temp_file.closed:
            self._temp_file.close()
        self.canceled = True

    def close_file(self):
        self._temp_file.close()

    def get_lock(self):
        return self._lock
            

class Downloader(threading.Thread):
    _download = None
    _url_handle = None
    _chunk = 0

    def __init__(self, url, save_destination, chunk=8190):
        super(Downloader, self).__init__()

        self._url_handle = urlopen(url)
        file_size = int(self._url_handle.info().get_all('Content-Length')[0])
        #file_size = int(self._url_handle.info().getheaders('Content-Length').strip())

        # url.split('/')[-1] should always be the file name
        self._download = Download(url.split('/')[-1], save_destination, file_size)

        self._chunk = chunk

    def run(self):
        while not self._download.completed():
            with self._download.get_lock():
                if not self._download.canceled:
                    self._download.append_data(self._url_handle.read(self._chunk))
                else:
                    break
        self._url_handle.close()
        self._download.close_file()

    def get_download(self):
        return self._download       

