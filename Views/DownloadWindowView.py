__author__ = 'jmann21'


from tkinter import *
from tkinter import ttk

from Views.IView import IView

class DownloadWindowView(IView):
    _update_methods = list()

    def __init__(self, refresh_rate=500):
        super().__init__()
        self._build_gui()
        self._refresh_rate = refresh_rate

    def _build_gui(self):
        self._root = Toplevel()
        self._root.title('Godot Launcher - Downloading Update')
        self._root.resizable(False, False)
        #self._root.protocol("WM_DELETE_WINDOW", exit)

        self._text_frame = ttk.Frame(self._root)
        self._text_frame.pack(fill=X, padx=5, pady=5, ipadx=0, ipady=0)

        self._status = ttk.Label(self._text_frame)
        self._status_var = StringVar() ####
        self._status.config(textvariable=self._status_var)
        self._status.pack(fill=X)
        self._status_var.set("Status: Downloading")

        self._speed = ttk.Label(self._text_frame)
        self._speed_var = StringVar()####
        self._speed.config(textvariable=self._speed_var)
        self._speed.pack(fill=X)

        self._progressbar_area = ttk.Frame(self._root)
        self._progressbar_area.pack(padx=5)

        self._progressbar = ttk.Progressbar(self._progressbar_area, orient=HORIZONTAL, length=300)
        self._progressbar.pack()
        self._progressbar.config(mode='determinate', maximum=100)
        self._progressbar_var = DoubleVar() ####
        self._progressbar.config(variable=self._progressbar_var)

        self._progress_percentage_var = StringVar()
        self._progress_percentage = ttk.Label(self._progressbar_area, textvariable=self._progress_percentage_var)
        self._progress_percentage.place(relx=0.5, rely=0.5, anchor='center')

        self._buttons = dict()
        self._buttons['close'] = ttk.Button(self._root)
        self._buttons['close'].pack(side=RIGHT, padx=5, pady=5)

    def _update(self):
        for method in self._update_methods:
            method()
        self._root.after(self._refresh_rate, self._update)

    # modified method based off
    # http://blogmag.net/blog/read/38/Print_human_readable_file_size
    # by Fred Cirera
    @staticmethod
    def _sizeof_fmt(num, suffix='B'):
        for unit in ['', 'K', 'M', 'G', 'T', 'P', 'E', 'Z']:
            if abs(num) < 1000.0:
                return "%3.1f %s%s/s" % (num, unit, suffix)
            num /= 1000.0
        return "%.1f %s%s/s" % (num, 'Y', suffix)

    def register_method(self, method):
        self._update_methods.append(method)

    def unregister_method(self, method):
        self._update_methods.remove(method)

    def set_status_downloading(self):
        self._status_var.set("Status: Downloading")
        self._buttons['close'].config(text='Cancel')

    def set_status_done(self):
        self._status_var.set("Status: Done")
        self._buttons['close'].config(text='Close')

    def set_download_speed(self, speed):
        self._speed_var.set("Speed: " + self._sizeof_fmt(speed))

    def set_progress(self, value):
        self._progressbar_var.set(value)
        self._progress_percentage_var.set(str(value) + '%')

    def mainloop(self):
        self._update()
        self._root.mainloop()

    def destroy(self):
        self._root.destroy()

    def set_button_action(self, **kwargs):
        arg_set = False

        if 'close' in kwargs or 'cancel' in kwargs:
            self._buttons['close'].config(command=kwargs['close'])
            arg_set = True

        if not arg_set:
            raise KeyError

class Runner:

    def __init__(self):
        self.downloader = Downloader("http://client.akamai.com/install/test-objects/10MB.bin", "C:/Users/Ryoh/Desktop/")
        self.download = self.downloader.get_download()
        self.window = DownloadWindowView()

        self.window.register_method(self.update)
        self.window.set_status_downloading()
        self.window.set_download_speed(0)
        self.window.set_button_action(close=self.close)

    def close(self):

        if not self.download.completed():
            with self.download.get_lock():
                self.download.cancel_download()

        self.window.destroy()

    def update(self):
        self.window.set_download_speed(self.download.get_speed())
        self.window.set_progress(self.download.get_percentage())

        if self.download.completed():
            self.window.set_status_done()

    def run(self):
        self.downloader.start()
        self.window.mainloop()